package com.example.leedstrinity.compscibok;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.paging.FirestorePagingAdapter;
import com.firebase.ui.firestore.paging.FirestorePagingOptions;
import com.firebase.ui.firestore.paging.LoadingState;
import com.google.firebase.firestore.DocumentSnapshot;

import org.w3c.dom.Document;

public class FirestoreAdapter extends FirestorePagingAdapter<AllBooks, FirestoreAdapter.BooksViewHolder>  {

    private OnListItemClick onListItemClick;


    public FirestoreAdapter(@NonNull FirestorePagingOptions<AllBooks> options, OnListItemClick onListItemClick) {
        super(options);
        this.onListItemClick = onListItemClick;
    }

    @Override
    protected void onBindViewHolder(@NonNull BooksViewHolder holder, int position, @NonNull AllBooks model) {
        holder.list_title.setText(model.getTitle());
        holder.list_author.setText(model.getAuthor());
        holder.list_isbn.setText(model.getIsbn());
    }

    @NonNull
    @Override
    public BooksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_single, parent, false);
        return new BooksViewHolder(view);
    }

    @Override
    protected void onLoadingStateChanged(@NonNull LoadingState state) {
        super.onLoadingStateChanged(state);
        switch (state){
            case LOADING_INITIAL:
                Log.d("PAGING_LOG", " Loading initial Books");
                break;
            case LOADING_MORE:
                Log.d("PAGING_LOG", " Loading next page");
                break;
            case FINISHED:
                Log.d("PAGING_LOG", " All Books loaded");
                break;
            case ERROR:
                Log.d("PAGING_LOG", " Error loading all Books");
                break;

            case LOADED:
                Log.d("PAGING.LOG", "Total Books Loaded : " + getItemCount());
                break;
        }
    }

    public class BooksViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView list_title;
        private TextView list_author;
        private TextView list_isbn;

        public BooksViewHolder(@NonNull View itemView) {
            super(itemView);

            list_title = itemView.findViewById(R.id.list_title);
            list_author = itemView.findViewById(R.id.list_author);
            list_isbn = itemView.findViewById(R.id.list_isbn);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onListItemClick.onItemClick(getItem(getAdapterPosition()), getAdapterPosition());
        }
    }

    public interface OnListItemClick {
        void onItemClick(DocumentSnapshot snapshot, int position);
    }

}
