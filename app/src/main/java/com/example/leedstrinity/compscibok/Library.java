package com.example.leedstrinity.compscibok;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.firebase.ui.firestore.paging.FirestorePagingAdapter;
import com.firebase.ui.firestore.paging.FirestorePagingOptions;
import com.firebase.ui.firestore.paging.LoadingState;
import com.google.api.Page;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class Library extends AppCompatActivity implements FirestoreAdapter.OnListItemClick {
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference book_db = db.collection("Books");
    private FirebaseFirestore firebaseFirestore;
    private RecyclerView mFirestoreList;

    private FirestorePagingAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        firebaseFirestore = FirebaseFirestore.getInstance();

        mFirestoreList = findViewById(R.id.firestore_list);

        Query query = firebaseFirestore.collection("Books");

        PagedList.Config config = new PagedList.Config.Builder()
                .setInitialLoadSizeHint(10)
                .setPageSize(3)
                .build();

        FirestorePagingOptions<AllBooks> options = new FirestorePagingOptions.Builder<AllBooks>()
                .setLifecycleOwner(this)
                .setQuery(query, config, AllBooks.class).build();

        adapter = new FirestoreAdapter(options, this);

        mFirestoreList.setHasFixedSize(true);
        mFirestoreList.setLayoutManager(new LinearLayoutManager(this));
        mFirestoreList.setAdapter(adapter);

    }

    @Override
    public void onItemClick(DocumentSnapshot snapshot, int position) {
        Log.d("ITEM CLICK", "Clicked the item : " + position + "and th ID : " + snapshot.getId());
    }
}