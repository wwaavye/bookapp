package com.example.leedstrinity.compscibok;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AddBooks extends AppCompatActivity {

    //Connection to firestore
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference book_db = db.collection("Books");
    private Button submitbook;
    private EditText titleEdittext;
    private EditText authorEdittext;
    private EditText isbnEdittext;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_books);
        titleEdittext=findViewById(R.id.title);
        authorEdittext=findViewById(R.id.author);
        isbnEdittext=findViewById(R.id.isbn);
        submitbook=findViewById(R.id.submitbook);



        submitbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = titleEdittext.getText().toString();
                String author = authorEdittext.getText().toString();
                String isbn = isbnEdittext.getText().toString();

                Map<String, String> bookData = new HashMap<>();
                bookData.put("title", title);
                bookData.put("author", author);
                bookData.put("isbn", isbn);

                    if (title.isEmpty()){
                        titleEdittext.setError("Book Title is required");
                        titleEdittext.requestFocus();
                        return;
                    }

                    if (author.isEmpty()){
                        authorEdittext.setError("Name of Author is required");
                        authorEdittext.requestFocus();
                        return;
                    }

                    if (isbn.isEmpty()){
                        isbnEdittext.setError("Isbn Number is required");
                        isbnEdittext.requestFocus();
                        return;
                    }


                book_db.add(bookData).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getApplicationContext(),"book added", Toast.LENGTH_LONG).show();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Error", "onFailure: "+ e);
                    }
                });
            }
        });

    }
}