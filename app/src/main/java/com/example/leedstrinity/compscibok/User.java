package com.example.leedstrinity.compscibok;

public class User {

    public String fullName, email;

    public User(){

    }

    public User(String fullName, String email){
        this.fullName = fullName;
        this.email = email;
    }
}
